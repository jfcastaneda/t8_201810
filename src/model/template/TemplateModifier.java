package model.template;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import model.data_structures.LinkedList;
import model.vo.VOPeso;

public class TemplateModifier {
	public static void dibujoRequerimiento1(double lat, double lon){
		try {
			File htmlTemplateFile = new File("template" + File.separator + "templateR1.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag = "lat: " + lat + ", lng: " + lon;
			htmlString = htmlString.replace("//$latLng", scriptTag);
			File newHtmlFile = new File("template" + File.separator +"mapaR1.html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void dibujoRequerimiento6Verdadero(double lat1, double lng1, double lat2, double lng2)
	{
		try {
			File htmlTemplateFile = new File("template" + File.separator + "templateR6Verdadero.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag1 = "lat: " + lat1 + ", lng: " + lng1;
			String scriptTag2 = "lat: " + lat2 + ", lng: " + lng2;
			htmlString = htmlString.replace("//$LatLng1", scriptTag1);
			htmlString = htmlString.replace("//$LatLng2", scriptTag2);
			File newHtmlFile = new File("template" + File.separator +"mapaR6Verdadero.html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void dibujoRequerimiento6(double lat1, double lng1, double lat2, double lng2)
	{
		try {
			File htmlTemplateFile = new File("template" + File.separator + "templateR6.html");
			String htmlString;
			htmlString = FileUtils.readFileToString(htmlTemplateFile);
			String scriptTag1 = "lat: " + lat1 + ", lng: " + lng1;
			String scriptTag2 = "lat: " + lat2 + ", lng: " + lng2;
			htmlString = htmlString.replace("//$LatLng1", scriptTag1);
			htmlString = htmlString.replace("//$LatLng2", scriptTag2);
			File newHtmlFile = new File("template" + File.separator +"mapaR6.html");
			FileUtils.writeStringToFile(newHtmlFile, htmlString);		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
