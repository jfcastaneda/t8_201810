package model.data_structures;


// TODO: Auto-generated Javadoc
/**
 * The Class Stack.
 *
 * @param <T> the generic type
 */
public class Stack<T> {

	// ------------------------------------------------------------------------------------
	// ------------------------- Atributos ------------------------------------------------
	// ------------------------------------------------------------------------------------

	/** The List. */
	private LinkedList<T> List;

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todo Constructor ---------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Instantiates a new stack.
	 */
	public Stack(){
		List = new LinkedList<T>();
	}

	// ------------------------------------------------------------------------------------
	// ------------------------- M�todos --------------------------------------------------
	// ------------------------------------------------------------------------------------

	/**
	 * Gets the head.
	 * @return the head
	 */
	public T getHead() {
		return List.getFirst();
	}

	/**
	 * Gets the size.
	 * @return the size
	 */
	public int getSize() {
		return List.size();
	}

	/**
	 * Push.
	 * @param item the item
	 */
	public void push(T item){
		List.addFirst(item);
	}

	/**
	 * Pop.
	 * @return the t
	 */
	public T pop(){
		return List.removeFirst();
	}

	/**
	 * Checks if is empty.
	 * @return true, if is empty
	 */
	public boolean isEmpty() {
		return getSize()==0;
	}
}
